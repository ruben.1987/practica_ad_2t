/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.AseguradoDao;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import pojos.Asegurado;

/**
 *
 * @author rubencete
 */
public class ReadFileAsegurado {

    public final String ASEGURADO = "src/files/Asegurados.txt";
    public final String ASEGURADO_RAF = "src/files/asegurado_raf.txt";

    public Asegurado insertAsegurado() {
        File file = new File("src/files/asegurado_raf.txt");
        Asegurado a = null;
        AseguradoDao asdao = new AseguradoDao();
        RandomAccessFile raf = null;
        try {
            String data = "";
            char[] array;
            int lenght = readFileWriteBinary(raf);
            System.out.println("Longitud de raf: " + lenght);
            raf = new RandomAccessFile(file, "rw");
            int position = 0;
            while (position < lenght) {
                a = new Asegurado();
                raf.seek(position);
                array = new char[9];
                data=readChar(array, raf);
                String dni = data;
                a.setDni(data);
                array = new char[20];
                data=readChar(array, raf);
                String nombre = data;
                a.setNombre(data);
                array = new char[50];
                data=readChar(array, raf);
                String apellido1 = data;
                a.setApellido1(data);
                array = new char[50];
                data=readChar(array, raf);
                String apellido2 = data;
                a.setApellido2(data);
                array = new char[10];
                data=readChar(array, raf);
                Date date = parseFecha(data);
                a.setFechaNacimiento(date);
                asdao.insertAsegurado(a);
                position += 278;
            }
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                raf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return a;
    }

    public String readChar(char[] array, RandomAccessFile raf) {
        String data="";
        try {
            for (int i = 0; i < array.length; i++) {
                array[i] = raf.readChar();
            }
            data = new String(array);
        } catch (IOException e) {
            e.printStackTrace();
        }
        data = data.replace("  ", "");
        return data;
    }

    public int readFileWriteBinary(RandomAccessFile raf) {
        File file = new File("src/files/asegurado_raf.txt");
        BufferedReader br = null;
        int lenght = 0;
        try {
            String line = "";
            String[] array;
            raf = new RandomAccessFile(file, "rw");
            br = new BufferedReader(new FileReader(ASEGURADO));
            while ((line = br.readLine()) != null) {
                raf.writeChars(line);
            }
            lenght = (int) raf.length();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
                raf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return lenght;
    }

    public Date parseFecha(String fecha) {
        Date date = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            date = format.parse(fecha);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
