/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.AseguradoDao;
import dao.PolizaDao;
import java.util.List;
import java.util.Scanner;
import pojos.Asegurado;
import pojos.Poliza;
import view.ViewApp;

/**
 *
 * @author rubencete
 */
public class GestionInformacion {

    private ViewApp view = new ViewApp();

    public void mostrarInformacionAsegurado(Scanner teclado) {
        GestionPoliza g = new GestionPoliza();
        PolizaDao pdao = new PolizaDao();
        DataAccess da = new DataAccess();
        AseguradoDao asdao = new AseguradoDao();
        int idAsegurado = 0;
        idAsegurado = g.insertIdAsegurado(teclado, idAsegurado);
        boolean exists = asdao.comprobarExistsAsegurado(idAsegurado);
        if (exists) {
            boolean aseguradoPoliza = pdao.existsPolizaAsegurado(idAsegurado);
            if (aseguradoPoliza) {
                Asegurado a = asdao.getAsegurado(idAsegurado);
                List<Poliza> p = pdao.getPolizasAsegurados(idAsegurado);
                view.mostrarDatosDetallePoliza(p,a);
            } else {
                System.out.println("El asegurado no tiene ninguna poliza.");
            }
        } else {
            System.out.println("El asegurado no existe");
        }

    }
}
