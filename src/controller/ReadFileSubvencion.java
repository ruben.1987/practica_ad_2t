/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.SubvencionDao;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.Normalizer;
import pojos.Subvencion;
import pojos.SubvencionId;

/**
 *
 * @author rubencete
 */
public class ReadFileSubvencion {

    public final String SUBVENCIONES = "src/files/Subvenciones.txt";
    public final String SUBVENCIONES_RAF = "src/files/Subvenciones_raf.txt";

public Subvencion insertSubvencion() {
        File file = new File(SUBVENCIONES_RAF);
        Subvencion s = null;
        SubvencionDao sbdao= new SubvencionDao();
        RandomAccessFile raf = null;
        try {
            String data = "";
            char[] array;
            int lenght = readFileWriteBinary(raf);
            raf = new RandomAccessFile(file, "rw");
            int position = 0;
            while (position < lenght) {
                s = new Subvencion();
                raf.seek(position);
                array = new char[10];
                data = readChar(array, raf);
                s.setAsegurado(sbdao.getOneAsegurado(Integer.parseInt(data)));
                array = new char[3];
                data = readChar(array, raf);
                s.setLinea(sbdao.getOneLinea(Integer.parseInt(data)));
                array = new char[3];
                data = readChar(array, raf);
                s.setImporte(Short.parseShort(data));
                array = new char[100];
                data = readChar(array, raf);
                s.setAsunto(data);
                s.setId(new SubvencionId(s.getAsegurado().getId(), s.getLinea().getCodigo()));
                sbdao.insertSubvencion(s);
                position += 232;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                raf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return s;
    }

    public String readChar(char[] array, RandomAccessFile raf) {
        String data = "";
        try {
            for (int i = 0; i < array.length; i++) {
                array[i] = raf.readChar();
            }
            data = new String(array);
        } catch (IOException e) {
            e.printStackTrace();
        }
        data = data.replace("  ", "");
        return data;
    }

    public int readFileWriteBinary(RandomAccessFile raf) {
        File file = new File(SUBVENCIONES_RAF);
        BufferedReader br = null;
        int lenght = 0;
        try {
            String line = "";
            String[] array;
            raf = new RandomAccessFile(file, "rw");
            br = new BufferedReader(new FileReader(SUBVENCIONES));
            while ((line = br.readLine()) != null) {
                raf.writeChars(line);
            }
            lenght = (int) raf.length();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
                raf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return lenght;
    }

    public String removeAccents(String line) {
        String normalizer = Normalizer.normalize(line, Normalizer.Form.NFD);
        String remove = normalizer.replaceAll("[^\\p{ASCII}]", "");
        return remove;
    }
}
