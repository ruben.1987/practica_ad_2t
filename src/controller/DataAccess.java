/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojos.Linea;
import pojos.Subvencion;
import practica_aseguradora_1.pkg0.NewHibernateUtil;

/**
 *
 * @author rubencete
 */
public class DataAccess {

    private Session session;
    private Transaction tx;

    public void initComponents() {
        session = NewHibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
    }

    public void hibernateException(HibernateException he) {
        tx.rollback();
        throw new HibernateException("Error en la capa de acceso a datos");
    }

    public boolean existsLineaAsociada(int idAsegurado, int idLinea) {
        boolean result = false;
        int id = 0;
        try {
            initComponents();
            result = (Long) session.createQuery("select count(*) from Poliza p where p.asegurado=" + idAsegurado+" and p.linea="+idLinea).uniqueResult() > 0;
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        }finally{
            session.close();
        }
        return result;
    }

    public boolean fechaInferiorContratacion(int id) {
        List<Linea> l = null;
        Date comprobar = null;
        try {
            initComponents();
            comprobar = new SimpleDateFormat("yyyy/MM/dd").parse("2013/06/30");
            Query query = session.createQuery("select l from Linea l where l.codigo=" + id);
            l = query.list();
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } catch (ParseException e) {
            e.printStackTrace();
        }finally{
            session.close();
        }
        return l.get(0).getFechaLimiteContratacion().before(comprobar);
    }

    public short comprobarSubvencion(int id,short importe) {
        List<Subvencion> s=null;
        boolean result=false;
        try {
            initComponents();
            result =(Long) session.createQuery("select count(*) from Subvencion s where s.asegurado="+id).uniqueResult()>0;
            if(result){
               s= session.createQuery("select s from Subvencion s where s.asegurado="+id).list();
            }
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        }finally{
            session.close();
        }
        if(result)importe -= (importe*s.get(0).getImporte())/100; 
        
        return importe;
    }
}
