/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.LineaDao;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import pojos.Linea;

/**
 *
 * @author rubencete
 */
public class ReadFileLinea {

    public final String LINEAS = "src/files/Lineas.txt";
    public final String LINEAS_RAF = "src/files/lineas_raf.txt";

    public Linea insertLinea() {
        File file = new File(LINEAS_RAF);
        LineaDao ldao = new LineaDao();
        Linea l = null;
        RandomAccessFile raf = null;
        try {
            String data = "";
            char[] array;
            int lenght = readFileWriteBinary(raf);
            System.out.println("Longitud de raf: " + lenght);
            raf = new RandomAccessFile(file, "rw");
            int position = 0;
            while (position < lenght) {
                l = new Linea();
                raf.seek(position);
                array = new char[3];
                data = readChar(array, raf);
                l.setCodigo(Integer.parseInt(data));
                String codigo = data;
                array = new char[100];
                data = readChar(array, raf);
                l.setDescriptivo(data);
                String descriptivo = data;
                String familia = String.valueOf(raf.readChar());
                l.setFamilia(familia);
                array = new char[10];
                data = readChar(array, raf);
                Date date = parseFecha(data);
                l.setFechaLimiteContratacion(date);
                ldao.insertLinea(l);
                position += 228;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                raf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return l;
    }

    public String readChar(char[] array, RandomAccessFile raf) {
        String data = "";
        try {
            for (int i = 0; i < array.length; i++) {
                array[i] = raf.readChar();
            }
            data = new String(array);
        } catch (IOException e) {
            e.printStackTrace();
        }
        data = data.replace("  ", "");
        return data;
    }

    public int readFileWriteBinary(RandomAccessFile raf) {
        File file = new File(LINEAS_RAF);
        BufferedReader br = null;
        int lenght = 0;
        try {
            String line = "";
            String[] array;
            raf = new RandomAccessFile(file, "rw");
            br = new BufferedReader(new FileReader(LINEAS));
            while ((line = br.readLine()) != null) {
                System.out.println(line);
                raf.writeChars(line);
            }
            lenght = (int) raf.length();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
                raf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return lenght;
    }

    public Date parseFecha(String fecha) {
        Date date = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            date = format.parse(fecha);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
