/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.AseguradoDao;
import dao.DetallePolizaDao;
import dao.LineaDao;
import dao.PolizaDao;
import exceptions.Exceptions;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import pojos.Asegurado;
import pojos.DetallePoliza;
import pojos.Linea;
import pojos.Poliza;
import view.ViewApp;

/**
 *
 * @author rubencete
 */
public class GestionPoliza {

    private ViewApp view = new ViewApp();

    public void addPoliza(Scanner teclado) {
        DataAccess da = new DataAccess();
        AseguradoDao asdo = new AseguradoDao();
        LineaDao ldao = new LineaDao();
        PolizaDao pdao = new PolizaDao();
        DetallePolizaDao dpdao = new DetallePolizaDao();
        int idAsegurado = 0, idLinea = 0;
        short importe = 0;
        idAsegurado = insertIdAsegurado(teclado, idAsegurado);
        idLinea = insertIdLinea(teclado, idLinea);
        if (!comprobarPoliza(idAsegurado, idLinea)) {
            importe = view.addPoliza(teclado);
            importe = da.comprobarSubvencion(idAsegurado, importe);
            int intRefencia = pdao.getLastReferencePoliza();
            int digito = createNewDigitoPoliza(intRefencia);
            Asegurado a = asdo.getAsegurado(idAsegurado);
            Linea l = ldao.getLinea(idLinea);
            String referencia = l.getFamilia() + String.valueOf(intRefencia);
            Date actual = fechaActual();
            Date posterior = fechaPosterior();
            Poliza p = createPolizaForDetallePoliza(referencia, a, l, digito);
            p.setImporte(BigDecimal.valueOf(importe));
            pdao.insertPoliza(p);
            DetallePoliza dp = new DetallePoliza(p, digito, actual, posterior);
            dpdao.insertAndGet(dp);
        } else {
            System.out.println("Ya tiene esa una poliza asociada");
        }
    }

    public int insertIdAsegurado(Scanner teclado, int idAsegurado) {
        AseguradoDao asdao = new AseguradoDao();
        boolean existsAsegurado = false;
        boolean comprobarNumero = true;
        do {
            try {
                System.out.println("Introduzca id de asegurado: ");
                idAsegurado = teclado.nextInt();
                teclado.nextLine();
                comprobarNumero = comprobarNumero(idAsegurado, comprobarNumero);
                existsAsegurado = asdao.comprobarExistsAsegurado(idAsegurado);
            } catch (InputMismatchException e) {
                teclado.nextLine();
                Exceptions.inputMismatch(e);
            }
        } while (!comprobarNumero || !existsAsegurado);
        return idAsegurado;
    }

    public int insertIdLinea(Scanner teclado, int idLinea) {
        LineaDao ldao = new LineaDao();
        boolean existsLinea = false;
        boolean comprobarNumero = true;
        do {
            try {
                System.out.println("Introduzca id de linea: ");
                idLinea = teclado.nextInt();
                teclado.nextLine();
                comprobarNumero = comprobarNumero(idLinea, comprobarNumero);
                existsLinea = ldao.comprobarExistsLinea(idLinea);
            } catch (InputMismatchException e) {
                teclado.nextLine();
                Exceptions.inputMismatch(e);
            }
        } while (!comprobarNumero || !existsLinea);
        return idLinea;
    }

    public boolean comprobarPoliza(int idAsegurado, int idLinea) {
        DataAccess da = new DataAccess();
        boolean fecha = da.fechaInferiorContratacion(idLinea);
        boolean result = da.existsLineaAsociada(idAsegurado, idLinea);
        return result;
    }

    public boolean comprobarNumero(int i, boolean comprobarNumero) {
        if (i <= 0) {
            comprobarNumero = false;
        } else {
            comprobarNumero = true;
        }
        return comprobarNumero;
    }

    public int getNumberReferencia(String referencia) {
        Matcher matcher = Pattern.compile("\\d").matcher(referencia);
        referencia = "";
        while (matcher.find()) {
            referencia += matcher.group();
        }
        System.out.println("Referencia numerica: " + referencia);
        return Integer.parseInt(referencia);
    }

    public int createNewDigitoPoliza(int reference) {
        PolizaDao pdao = new PolizaDao();
        int digito = reference % 6;
        return digito;
    }

    public Date fechaActual() {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy/MM/dd").parse("2015/06/30");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public Date fechaPosterior() {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy/MM/dd").parse("2016/06/30");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public Poliza createPolizaForDetallePoliza(String referencia, Asegurado a, Linea l, int digito) {
        Poliza p = new Poliza(referencia, a, l, digito);
        return p;
    }
}
