/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica_aseguradora_1.pkg0;

import controller.GestionInformacion;
import controller.GestionPoliza;
import dao.AseguradoDao;
import exceptions.Exceptions;
import java.util.InputMismatchException;
import java.util.Scanner;
import view.ViewApp;

/**
 *
 * @author rubencete
 */
public class Practica_aseguradora_10 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        ViewApp view = new ViewApp();
        GestionPoliza gp = new GestionPoliza();
        AseguradoDao asdo = new AseguradoDao();
        boolean loadData = false, inside = true, comprobarNumero = false;
        int opcion = 0;

        if (asdo.comprobarDatosInicio()) {
            inside = view.cargarDatos(teclado);
        }
        if (inside) {
            do {
                try {
                    System.out.println("¿Que desea hacer?");
                    System.out.println("1.Contratar poliza.");
                    System.out.println("2.Consultar datos de asegurados.");
                    opcion = teclado.nextInt();
                    teclado.nextLine();
                    comprobarNumero = true;
                } catch (InputMismatchException e) {
                    comprobarNumero = false;
                    teclado.nextLine();
                    Exceptions.inputMismatch(e);
                }
                switch (opcion) {
                    case 1:
                        GestionPoliza g = new GestionPoliza();
                        g.addPoliza(teclado);
                        break;
                    case 2:
                        GestionInformacion gi = new GestionInformacion();
                        gi.mostrarInformacionAsegurado(teclado);
                        break;
                    default:
                        System.out.println("Opcion inexistente!!!");
                }
            } while (opcion != 0 || !comprobarNumero);
        }
    }

}
