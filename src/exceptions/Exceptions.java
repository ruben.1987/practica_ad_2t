/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

import java.util.InputMismatchException;

/**
 *
 * @author rubencete
 */
public class Exceptions extends Exception{
    
    public static void inputMismatch(InputMismatchException in){
        System.err.println("Introduzca solo numeros mayores que cero :)");
    }
}
