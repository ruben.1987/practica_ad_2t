/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.GestionPoliza;
import controller.ReadFileAsegurado;
import controller.ReadFileLinea;
import controller.ReadFileSubvencion;
import dao.DetallePolizaDao;
import dao.LineaDao;
import exceptions.Exceptions;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import pojos.Asegurado;
import pojos.DetallePoliza;
import pojos.Linea;
import pojos.Poliza;

/**
 *
 * @author rubencete
 */
public class ViewApp {

    public boolean cargarDatos(Scanner teclado) {
        boolean loadData = false, inside = true, comprobarNumeros = false;
        int opcion = 0;
        do {
            do {
                try {
                    System.out.println("¿Desea cargar los datos?");
                    System.out.println("1.SI");
                    System.out.println("2.NO");
                    opcion = teclado.nextInt();
                    teclado.nextLine();
                    comprobarNumeros = true;
                } catch (InputMismatchException e) {
                    teclado.nextLine();
                    comprobarNumeros = false;
                    Exceptions.inputMismatch(e);
                }
            } while (!comprobarNumeros);

            comprobarNumeros = false;

            switch (opcion) {
                case 1:
                    ReadFileAsegurado rfa = new ReadFileAsegurado();
                    rfa.insertAsegurado();
                    ReadFileLinea rfl = new ReadFileLinea();
                    rfl.insertLinea();
                    ReadFileSubvencion rfs = new ReadFileSubvencion();
                    rfs.insertSubvencion();
                    loadData = true;
                    break;
                case 2:
                    do {
                        try {
                            System.out.println("Si no carga datos debera salir del programa.");
                            System.out.println("¿Desea salir?");
                            System.out.println("1.SI");
                            System.out.println("2.NO");
                            opcion = teclado.nextInt();
                            teclado.nextLine();
                            comprobarNumeros = true;
                        } catch (InputMismatchException e) {
                            teclado.nextLine();
                            comprobarNumeros = false;
                            Exceptions.inputMismatch(e);
                        }
                    } while (!comprobarNumeros);
                    switch (opcion) {
                        case 1:
                            System.out.println("Hasta pronto!!!!");
                            loadData = true;
                            break;
                        case 2:
                            loadData = false;
                            inside = false;
                            break;
                        default:
                            System.out.println("Opcion inexistente!!");
                    }
                    break;
                default:
                    System.out.println("Opcion inexistente!!");
            }
        } while (!loadData);
        if (inside) {
            return true;
        } else {
            return false;
        }
    }

    public short addPoliza(Scanner teclado) {
        GestionPoliza g = new GestionPoliza();
        boolean comprobar = false;
        short importe = 0;
        do {
            try {
                System.out.println("Seleccione importe de la poliza: ");
                importe = teclado.nextShort();
                teclado.nextLine();
                comprobar = comprobarNumero(importe, comprobar);
            } catch (InputMismatchException e) {
                teclado.nextLine();
                comprobar = false;
                Exceptions.inputMismatch(e);
            }
        } while (!comprobar || importe <= 0);
        return importe;
    }

    public boolean comprobarNumero(short i, boolean comprobar) {
        if (i <= 0) {
            comprobar = false;
        } else {
            comprobar = true;
        }
        return comprobar;
    }

    public void mostrarAsegurado(Asegurado a) {
        System.out.println("Datos del asegurado: ");
        System.out.println("Nombre completo: " + a.getNombre() + " " + a.getApellido1() + " " + a.getApellido2());
        System.out.println("Fecha de nacimiento: " + a.getFechaNacimiento());
        System.out.println("D.N.I. :" + a.getDni());
    }

    public void mostrarLinea(Linea l) {
        System.out.println("Datos de la Linea: ");
        System.out.println("Familia: " + l.getFamilia());
        System.out.println("Descripcion: " + l.getDescriptivo());
    }

    public void mostrarPoliza(Poliza p) {
        System.out.println("Poliza contratada: ");
        System.out.println("Importe: " + p.getImporte());
    }

    public void mostraDetallePoliza(DetallePoliza dp) {
        System.out.println("Fecha de alta: " + dp.getFechaAlta());
        System.out.println("Fecha de vencimiento: " + dp.getFechaVencimiento());
    }

    public void mostrarDatosDetallePoliza(List<Poliza> p, Asegurado a) {
        DetallePolizaDao dpdao = new DetallePolizaDao();
        LineaDao ldao = new LineaDao();
        DetallePoliza dp = null;
        Linea l = null;
        mostrarAsegurado(a);
        for (Poliza pl : p) {
            l = ldao.getLinea(pl.getLinea().getCodigo());
            mostrarLinea(l);
            mostrarPoliza(pl);
            dp = dpdao.getDetallesPoliza(pl.getReferencia());
            mostraDetallePoliza(dp);
        }
    }
}
