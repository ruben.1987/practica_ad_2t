/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojos.DetallePoliza;
import practica_aseguradora_1.pkg0.NewHibernateUtil;

/**
 *
 * @author rubencete
 */
public class DetallePolizaDao {

    private Session session;
    private Transaction tx;

    public void initComponents() {
        session = NewHibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
    }

    public void hibernateException(HibernateException he) {
        tx.rollback();
        throw new HibernateException("Error en la capa de acceso a datos", he);
    }

    public DetallePoliza insertAndGet(DetallePoliza dp) {
        try {
            initComponents();
            session.save(dp);
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } finally {
            session.close();
        }
        return dp;
    }

    public DetallePoliza getDetallesPoliza(String referencia) {
        List<DetallePoliza> dp = null;
        try {
            initComponents();
            Query query = session.createQuery("select dp from DetallePoliza dp where dp.referencia=:filter");
            query.setParameter("filter", referencia);
            dp = query.list();
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } finally {
            session.close();
        }
        return dp.get(0);
    }
}
