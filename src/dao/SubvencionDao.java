/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojos.Asegurado;
import pojos.Linea;
import pojos.Subvencion;
import practica_aseguradora_1.pkg0.NewHibernateUtil;

/**
 *
 * @author rubencete
 */
public class SubvencionDao {

    private Session session;
    private Transaction tx;

    public void initComponents() {
        session = NewHibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
    }

    public void hibernateException(HibernateException he) {
        tx.rollback();
        throw new HibernateException("Error en la capa de acceso a datos");
    }

    public int insertSubvencion(Subvencion s) {
        int i = 0;
        try {
            initComponents();
            session.save(s);
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                hibernateException(e);
            }
        }
        return i;
    }
    
    public Linea getOneLinea(int id) {
        Linea l = null;
        try {
            initComponents();
            l = (Linea) session.load(Linea.class, id);
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                hibernateException(e);

            }
        }
        return l;
    }
    
    public Asegurado getOneAsegurado(int id) {
        Asegurado a = null;
        try {
            initComponents();
            a= (Asegurado)session.load(Asegurado.class, id);
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                hibernateException(e);
            }
        }
        return a;
    }
}
