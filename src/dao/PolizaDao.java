/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import controller.GestionPoliza;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojos.Poliza;
import practica_aseguradora_1.pkg0.NewHibernateUtil;

/**
 *
 * @author rubencete
 */
public class PolizaDao {

    private Session session;
    private Transaction tx;

    public void initComponents() {
        session = NewHibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
    }

    public void hibernateException(HibernateException he) {
        tx.rollback();
        throw new HibernateException("Error en la capa de acceso a datos", he);
    }

    public void insertPoliza(Poliza p) {
        try {
            initComponents();
            session.save(p);
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } finally {
            session.close();
        }
    }

    public int getLastReferencePoliza() {
        Poliza p = null;
        GestionPoliza g = new GestionPoliza();
        boolean exists = false;
        try {
            initComponents();
            exists = (long) session.createQuery("select count(*) from Poliza").uniqueResult() > 0;
            Query query = session.createQuery("select p from Poliza p order by referencia desc");
            query.setMaxResults(1);
            p = (Poliza) query.uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } finally {
            session.close();
        }
        if (!exists) {
            return 250;
        } else {
            return g.getNumberReferencia(p.getReferencia()) + 1;
        }
    }

    public boolean existsPolizaAsegurado(int id) {
        boolean result = false;
        try {
            initComponents();
            result = (Long) session.createQuery("select count(*) from Poliza p where p.asegurado=" + id).uniqueResult() > 0;
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        }
        return result;
    }
    
    public List<Poliza> getPolizasAsegurados(int id) {
        List<Poliza> p=null;
        try {
            initComponents();
            p =session.createQuery("select p from Poliza p where p.asegurado=" + id).list();
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        }
        return p;
    }
}
