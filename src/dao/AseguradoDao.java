/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojos.Asegurado;
import practica_aseguradora_1.pkg0.NewHibernateUtil;

/**
 *
 * @author rubencete
 */
public class AseguradoDao {

    private Session session;
    private Transaction tx;

    public void initComponents() {
        session = NewHibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
    }

    public void hibernateException(HibernateException he) {
        tx.rollback();
        throw new HibernateException("Error en la capa de acceso a datos", he);
    }

    public int insertAsegurado(Asegurado a) {
        int i = 0;
        try {
            initComponents();
            session.save(a);
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                hibernateException(e);
            }
        }
        return i;
    }
    
    public Asegurado getAsegurado(int id){
        Asegurado a=null;
        try {
            initComponents();
            a=(Asegurado)session.get(Asegurado.class, id);
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                hibernateException(e);
            }
        }
        return a;
    }
    
    public boolean comprobarExistsAsegurado(int id) {
        boolean result=false;
        try {
            initComponents();
            result=(Long)session.createQuery("select count(*) from Asegurado a where a.id="+id).uniqueResult()>0;
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                hibernateException(e);
            }
        }
        if(!result)System.out.println("El ID de usuario no existe");
        return result;
    }
    
    public boolean comprobarDatosInicio() {
        List<Asegurado> a=null;
        try {
            initComponents();
            a=session.createQuery("from Asegurado").list();
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                hibernateException(e);
            }
        }
        return a.isEmpty();
    }
}
