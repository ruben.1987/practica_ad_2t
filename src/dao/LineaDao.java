/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojos.Linea;
import practica_aseguradora_1.pkg0.NewHibernateUtil;

/**
 *
 * @author rubencete
 */
public class LineaDao {

    private Session session;
    private Transaction tx;

    public void initComponents() {
        session = NewHibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
    }

    public void hibernateException(HibernateException he) {
        tx.rollback();
        throw new HibernateException("Error en la capa de acceso a datos");
    }

    public int insertLinea(Linea l) {
        int i = 0;

        try {
            initComponents();
            session.save(l);
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                hibernateException(e);
            }
        }

        return i;
    }
    
    public boolean comprobarExistsLinea(int id) {
        boolean result=false;
        try {
            initComponents();
            result= (Long) session.createQuery("select count(*) from Linea l where l.codigo="+id).uniqueResult()>0;
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                hibernateException(e);
            }
        }
        if(!result)System.out.println("El ID de linea no existe");
        return result;
    }

    public Linea getLinea(int id) {
        Linea l = null;
        try {
            initComponents();
            l = (Linea) session.get(Linea.class, id);
            tx.commit();
        } catch (HibernateException e) {
            hibernateException(e);
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                hibernateException(e);
            }
        }
        return l;
    }
}
